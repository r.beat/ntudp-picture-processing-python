import os
import cv2 as cv
import numpy as np  
import matplotlib.pyplot as plt

# file name
SURNAME_NAME_FATHERNAME = "Grinberg Rodion Michalovich" 

IMG_TOP = 30
IMG_DOWN = 12
IMG_MIDLE_VERTICAL = 100 - IMG_DOWN - IMG_TOP

IMG_LEFT = 28
IMG_RIGHT = 24
IMG_MIDLE_HORIZANTAL = 100 - IMG_LEFT - IMG_RIGHT

def main():
    '''MAIN'''

    # import image
    img = cv.imread('image.png')

    # check type and shape of image 
    print(type(img))
    height, width, ch = img.shape
    print(height)
    print(width)

    # top - mid - down
    top = int(height/100*IMG_TOP)
    mid = int(height/100*IMG_MIDLE_VERTICAL) + top

    crop = img[0:top, 0:width]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_top.jpg', crop)

    crop = img[top:mid, 0:width]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_mid_vertical.jpg', crop)

    crop = img[mid:height, 0:width]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_down.jpg', crop)


    # top - mid - down
    left = int(width/100*IMG_LEFT)
    mid = int(width/100*IMG_MIDLE_HORIZANTAL) + left

    crop = img[0:height, 0:left]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_left.jpg', crop)

    crop = img[0:height, left:mid]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_mid_horizantal.jpg', crop)

    crop = img[0:height, mid:width]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_right.jpg', crop)


    # 2.2

    top =   int(height/100 * 25)
    down =  int(height - height/100 * 26)
    left =  int(width/100 * 16)
    right = int(width - width/100 * 12)

    crop = img[left:right, top:down]
    cv.imshow('crop', crop) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_crop.jpg', crop)



if __name__ == "__main__":
    main()
