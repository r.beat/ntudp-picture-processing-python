import os
import cv2 as cv
import numpy as np  
import matplotlib.pyplot as plt

# file name
SURNAME_NAME_FATHERNAME = "Grinberg Rodion Michalovich" 


def ShowBinary(img, step: int, title: str):
    '''Binary method'''
    ret, binary = cv.threshold(img, step, 255, cv.THRESH_BINARY)
    print(f"{title} | {ret}") 
    cv.imshow(title, binary)
    cv.imwrite(f'{title}.jpg', binary)


def ShowBinaryOtsu(img, step: int, title: str):
    '''OSTU method'''
    ret, binary = cv.threshold(img, step, 255, cv.THRESH_OTSU)
    print(f"{title} | {ret}") 
    cv.imshow(title, binary)
    cv.imwrite(f'{title}.jpg', binary)


def ShowBinaryAdaptive(img, step: int, block_size: int, title: str):
    '''Adaptive method'''
    binary = cv.adaptiveThreshold(img, step, cv.ADAPTIVE_THRESH_MEAN_C, cv.THRESH_BINARY, block_size, 4)  
    print(f"{title}") 
    cv.imshow(title, binary)
    cv.imwrite(f'{title}.jpg', binary)


def ShowBinaryCany(img, step: int, title: str):
    '''Cany method'''
    binary = cv.Canny(img,step,200)
    print(f"{title}") 
    cv.imshow(title, binary)
    cv.imwrite(f'{title}.jpg', binary)



def main():
    '''MAIN'''

    # import image
    img = cv.imread('image.png')

    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    cv.imshow('gray', gray)

    ShowBinary(gray, 50, "Binary (50)")
    ShowBinary(gray, 100, "Binary (100)")
    ShowBinary(gray, 200, "Binary (200)")

    ShowBinaryOtsu(gray, 50, "Otsu (50)")
    ShowBinaryOtsu(gray, 150, "Otsu (150)")
    ShowBinaryOtsu(gray, 200, "Otsu (200)")

    ShowBinaryAdaptive(gray, 100, 15, "Adaptive (100)")
    ShowBinaryAdaptive(gray, 150, 5, "Adaptive (150)")
    ShowBinaryAdaptive(gray, 200, 41, "Adaptive (200)")

    ShowBinaryCany(gray, 50, "Cany (50)")
    ShowBinaryCany(gray, 120, "Cany (120)")
    ShowBinaryCany(gray, 200, "Cany (200)")
    
    # cv.waitKey(0)




if __name__ == "__main__":
    main()
