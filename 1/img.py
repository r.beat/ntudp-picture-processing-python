import os
import cv2 as cv
import numpy as np  
import matplotlib.pyplot as plt

# file name
SURNAME_NAME_FATHERNAME = "Grinberg Rodion Michalovich" 

def main():
    '''MAIN'''

    # import image
    img = cv.imread('image.png')

    # check type and shape of image 
    print(type(img))
    print(img.shape)

    # show image using cv and save it as .jpg file
    cv.imshow('Original image', img) 
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}.jpg', img)

    # show image using matlab and save in as .jpg file
    img_rgb = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    plt.imshow(img_rgb) 
    plt.show()
    cv.imwrite(f'{SURNAME_NAME_FATHERNAME}_rgb.jpg', img_rgb)


if __name__ == "__main__":
    main()
